﻿using Business.DomainModel;
using Business.Services;
using Data.Contract;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;

using Dom = Business.DomainModel;

namespace Business.Tests.Services
{
    [TestClass]
    public class RestaurantServiceTest
    {
        // Test naming convention
        //    UnitOfWork_StateUnderTest_ExpectedBehavior

        [TestMethod]
        public void GetAll_DataToDomainObjectConversion_CorrectNumberOfObjectsReturnedWithCorrectProperties()
        {
            // ARRANGE
            var repositoryMock = new Mock<IRestaurantRepository>();
            var someRestaurants = GetSomeRestaurantDataModelEntities().ToArray();
            repositoryMock.Setup(m => m.GetAll()).Returns(someRestaurants);

            var service = new RestaurantService(repositoryMock.Object);

            // ACT
            var result = service.GetAll().ToArray();

            // ASSERT
            string errDesc;
            Assert.AreEqual(3, result.Length);
            Assert.IsTrue(Match(someRestaurants[0], result[0], out errDesc), errDesc);
            Assert.IsTrue(Match(someRestaurants[1], result[1], out errDesc), errDesc);
            Assert.IsTrue(Match(someRestaurants[2], result[2], out errDesc), errDesc);
        }

        [TestMethod]
        public void GetById_ObjectFoundAndConverted_CorrectObjectReturnedWithCorrectProperties()
        {
            // ARRANGE
            var repositoryMock = new Mock<IRestaurantRepository>();
            var id = 1;
            var someRestaurants = GetSomeRestaurantDataModelEntities().ToArray();
            repositoryMock.Setup(m => m.GetById(id)).Returns(someRestaurants[0]);

            var service = new RestaurantService(repositoryMock.Object);

            // ACT
            var restaurant = service.GetById(id);

            // ASSERT
            string errDesc;
            Assert.IsTrue(Match(someRestaurants[0], restaurant, out errDesc), errDesc);
        }

        [TestMethod]
        public void Update_SimpleUpdate_ObjectConvertedCorrectly()
        {
            // ARRANGE
            var repositoryMock = new Mock<IRestaurantRepository>();
            var service = new RestaurantService(repositoryMock.Object);
            var domainRestaurant = GetDomainModelRestaurant();

            Data.Restaurant convertedRestaurant = null;
            repositoryMock.Setup(m => m.Update(It.IsAny<Data.Restaurant>()))
                          .Callback<Data.Restaurant>(r => convertedRestaurant = r);

            // ACT
            service.Update(domainRestaurant);

            // ASSERT
            string errDesc;
            Assert.IsTrue(Match(convertedRestaurant, domainRestaurant, out errDesc), errDesc);
        }

        #region Test Helpers

        private IEnumerable<Data.Restaurant> GetSomeRestaurantDataModelEntities()
        {
            return new []
            {
                new Data.Restaurant
                {
                    Id = 1,
                    Title = "Restaurant1",
                    PhoneNumberText = "(02) 1234 1234",
                    PhoneNumberKey = 212341234,
                    CuisineId = 1, // (Asian)
                    HeadChefName = "Bob",
                    RatingFood = 5,
                    RatingWine = 6,
                    StreetAddress = "123 Tree St",
                    AddressSuburb = "Lane Cove",
                    AddressState = "NSW",
                    AddressPostCode = "2066",
                    Latitude = 12.34,
                    Longitude = 56.78,
                    WebAddress = "http://www.something.com",
                    ReviewText = "really good!",
                    Photo = new byte[] { 0x1, 0x2, 0x3 }
                },
                new Data.Restaurant
                {
                    Id = 2,
                    Title = "Restaurant2",
                    PhoneNumberText = "(02) 1234 9876",
                    PhoneNumberKey = 212349876,
                    CuisineId = 2, // (Chinese)
                    HeadChefName = "Sam",
                    RatingFood = 7,
                    RatingWine = 8,
                    StreetAddress = "45 Tree St",
                    AddressSuburb = "Sydney",
                    AddressState = "NSW",
                    AddressPostCode = "2000",
                    Latitude = 142.34,
                    Longitude = 576.78,
                    WebAddress = "http://www.somethingelse.com",
                    ReviewText = "not so good!",
                    Photo = new byte[] { 0x4, 0x5, 0x6 }
                },
                new Data.Restaurant
                {
                    Id = 5,
                    Title = "Restaurant 5",
                    PhoneNumberText = "(02) 9000 6000",
                    PhoneNumberKey = 290006000,
                    CuisineId = 16, // (Mexican)
                    HeadChefName = "Sally",
                    RatingFood = 12,
                    RatingWine = 30,
                    StreetAddress = "56 Long Lane",
                    AddressSuburb = "Sutherland",
                    AddressState = "NSW",
                    AddressPostCode = "1234",
                    Latitude = 66.77,
                    Longitude = 88.88,
                    WebAddress = "http://www.food.com",
                    ReviewText = "above average",
                    Photo = new byte[] { 0x1, 0x2, 0x3 }
                }
            };
        }

        private Dom.Restaurant GetDomainModelRestaurant()
        {
            return new Dom.Restaurant
            {
                Id = 1,
                Title = "Restaurant1",
                PhoneNumberText = "(02) 1234 1234",
                PhoneNumberKey = 212341234,
                Cuisine = CuisineEnum.Asian,
                HeadChefName = "Bob",
                RatingFood = 5,
                RatingWine = 6,
                StreetAddress = "123 Tree St",
                AddressSuburb = "Lane Cove",
                AddressState = "NSW",
                AddressPostCode = "2066",
                Latitude = 12.34,
                Longitude = 56.78,
                WebAddress = "http://www.something.com",
                ReviewText = "really good!",
                //Photo = new byte[] { 0x1, 0x2, 0x3 }
            };
        }

        private bool Match(Data.Restaurant dataRestaurant, Dom.Restaurant domainRestaurant, out string errorDescription)
        {
            errorDescription = "";

            if (dataRestaurant == null || domainRestaurant == null)
            {
                errorDescription = "object null";
                return false;
            }

            // TODO compare photos once added to domain entity

            if (dataRestaurant.Id != domainRestaurant.Id)
            {
                errorDescription = "Id doesn't match";
                return false;
            }

            if (!String.Equals(dataRestaurant.Title, domainRestaurant.Title))
            {
                errorDescription = "Title doesn't match";
                return false;
            }

            if (!String.Equals(dataRestaurant.PhoneNumberText, domainRestaurant.PhoneNumberText))
            {
                errorDescription = "PhoneNumberText doesn't match";
                return false;
            }

            if (dataRestaurant.PhoneNumberKey != domainRestaurant.PhoneNumberKey)
            {
                errorDescription = "PhoneNumberKey doesn't match";
                return false;
            }

            if (dataRestaurant.CuisineId != (int) domainRestaurant.Cuisine)
            {
                errorDescription = "Cuisine doesn't match";
                return false;
            }

            if (!String.Equals(dataRestaurant.HeadChefName, domainRestaurant.HeadChefName))
            {
                errorDescription = "HeadChefName doesn't match";
                return false;
            }

            if (dataRestaurant.RatingFood != domainRestaurant.RatingFood)
            {
                errorDescription = "RatingFood doesn't match";
                return false;
            }

            if (dataRestaurant.RatingWine != domainRestaurant.RatingWine)
            {
                errorDescription = "RatingWine doesn't match";
                return false;
            }

            if (!String.Equals(dataRestaurant.StreetAddress, domainRestaurant.StreetAddress))
            {
                errorDescription = "StreetAddress doesn't match";
                return false;
            }

            if (!String.Equals(dataRestaurant.AddressSuburb, domainRestaurant.AddressSuburb))
            {
                errorDescription = "AddressSuburb doesn't match";
                return false;
            }

            if (!String.Equals(dataRestaurant.AddressState, domainRestaurant.AddressState))
            {
                errorDescription = "AddressState doesn't match";
                return false;
            }

            if (!String.Equals(dataRestaurant.AddressPostCode, domainRestaurant.AddressPostCode))
            {
                errorDescription = "AddressPostCode doesn't match";
                return false;
            }

            if (dataRestaurant.Latitude != domainRestaurant.Latitude)
            {
                errorDescription = "Latitude doesn't match";
                return false;
            }

            if (dataRestaurant.Longitude != domainRestaurant.Longitude)
            {
                errorDescription = "Longitude doesn't match";
                return false;
            }

            if (!String.Equals(dataRestaurant.WebAddress, domainRestaurant.WebAddress))
            {
                errorDescription = "WebAddress doesn't match";
                return false;
            }

            if (!String.Equals(dataRestaurant.ReviewText, domainRestaurant.ReviewText))
            {
                errorDescription = "ReviewText doesn't match";
                return false;
            }

            return true;
        }

        #endregion Test Helpers
    }
}
