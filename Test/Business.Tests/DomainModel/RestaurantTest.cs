﻿using System;
using Business.DomainModel;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Business.Tests.DomainModel
{
    [TestClass]
    public class RestaurantTest
    {
        // Test naming convention
        //    UnitOfWork_StateUnderTest_ExpectedBehavior

        [TestMethod]
        public void FormatPhoneNumber_NonDigitCharacters_CharactersRemoved()
        {
            // ARRANGE
            const string input = "(02)sdf1234 gg fe 5 kj7l 3mn8xxx";
            const string expectedOutput = "(02) 1234 5738";

            // ACT
            var actualOutput = Restaurant.FormatPhoneNumber(input);

            // ASSERT
            Assert.AreEqual(expectedOutput, actualOutput);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void FormatPhoneNumber_NotEnoughSignificantDigits_ThrowsException()
        {
            // ARRANGE
            const string input = "(02)sdf134 gg fe 5 kj7l 3mn8xxx";

            // ACT / ASSERT
            var actualOutput = Restaurant.FormatPhoneNumber(input);
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void CalculatePhoneNumberKey_PhoneNumberTextInvalid_ThrowsException()
        {
            // ARRANGE
            var restaurant = new Restaurant { PhoneNumberText = "abc" };

            // ACT / ASSERT
            restaurant.CalculatePhoneNumberKey();
        }

        [TestMethod]
        public void CalculatePhoneNumberKey_PhoneNumberTextValid_CalculationSuccessful()
        {
            // ARRANGE
            var restaurant = new Restaurant { PhoneNumberText = "(02) 1234 5678" };

            // ACT
            restaurant.CalculatePhoneNumberKey();

            // ASSERT
            Assert.AreEqual(212345678u, restaurant.PhoneNumberKey);
        }
    }
}
