﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using MvcApplication.Models;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MvcApplication.Tests.Models
{
    [TestClass]
    public class EditRestaurantViewModelTest
    {
        // Test naming convention
        //    UnitOfWork_StateUnderTest_ExpectedBehavior

        [TestMethod]
        public void PhoneNumberValidation_ValidPhoneNumber_ValidationSucceeds()
        {
            // ARRANGE
            const string value = "(02) 1234 1234";
            var vm = new EditRestaurantViewModel { PhoneNumberText = value };

            var results = new List<ValidationResult>();
            var vc = new ValidationContext(vm, null, null) { MemberName = "PhoneNumberText" };

            // ACT
            var isValid = Validator.TryValidateProperty(value, vc, results);

            // ASSERT
            Assert.IsTrue(isValid);
        }

        [TestMethod]
        public void PhoneNumberValidation_LessThan9Digits_ValidationFailed()
        {
            // ARRANGE
            const string value = "(02) 1234 123";
            var vm = new EditRestaurantViewModel { PhoneNumberText = value };

            var results = new List<ValidationResult>();
            var vc = new ValidationContext(vm, null, null) { MemberName = "PhoneNumberText" };

            // ACT
            var isValid = Validator.TryValidateProperty(value, vc, results);

            // ASSERT
            Assert.IsFalse(isValid);
        }

        [TestMethod]
        public void PhoneNumberValidation_PhoneNumberPaddedWithNonDigits_ValidationSucceeds()
        {
            // ARRANGE
            const string value = "(02)a1f2&34@ u12jj3xx4x";
            var vm = new EditRestaurantViewModel { PhoneNumberText = value };

            var results = new List<ValidationResult>();
            var vc = new ValidationContext(vm, null, null) { MemberName = "PhoneNumberText" };

            // ACT
            var isValid = Validator.TryValidateProperty(value, vc, results);

            // ASSERT
            Assert.IsTrue(isValid);
        }
    }
}
