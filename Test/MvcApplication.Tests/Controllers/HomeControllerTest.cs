﻿using System.Linq;
using System.Web.Mvc;
using Business.Contract;
using Business.DomainModel;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using MvcApplication.Controllers;
using MvcApplication.Models;

namespace MvcApplication.Tests.Controllers
{
    [TestClass]
    public class HomeControllerTest
    {
        // Test naming convention
        //    UnitOfWork_StateUnderTest_ExpectedBehavior

        [TestMethod]
        public void Index_SimpleGET_ViewResultReturned()
        {
            // ARRANGE
            var serviceMock = new Mock<IRestaurantService>();
            var controller = new HomeController(serviceMock.Object);

            // ACT
            var result = controller.Index() as ViewResult;

            // ASSERT
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void Index_SimpleGET_CorrectEntitiesReturned()
        {
            // ARRANGE
            var serviceMock = new Mock<IRestaurantService>();

            serviceMock.Setup(m => m.GetAll()).Returns(
                new[]
                {
                    new Restaurant {Title = "Restaurant 1"},
                    new Restaurant {Title = "Restaurant 2"},
                    new Restaurant {Title = "Restaurant 3"}
                });

            var controller = new HomeController(serviceMock.Object);

            // ACT
            var result = controller.Index() as ViewResult;
            var vm = result.Model as RestaurantListViewModel;

            // ASSERT
            Assert.IsNotNull(vm);
            Assert.AreEqual(3, vm.Restaurants.Count());
        }
    }
}
