﻿using Business.Contract;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using MvcApplication.Controllers;
using System.Net;
using System.Web.Mvc;

namespace MvcApplication.Tests.Controllers
{
    [TestClass]
    public class RestaurantControllerTest
    {
        // Test naming convention
        //    UnitOfWork_StateUnderTest_ExpectedBehavior

        [TestMethod]
        public void Edit_RestaurantWithIdDoesntExist_NotFoundStatusCodeReturned()
        {
            // ARRANGE
            var serviceMock = new Mock<IRestaurantService>();
            // serviceMock will return null for a method that hasn't been setup

            var controller = new RestaurantController(serviceMock.Object);

            // ACT
            var result = controller.Edit(1);

            // ASSERT
            Assert.IsInstanceOfType(result, typeof(HttpStatusCodeResult));
            Assert.AreEqual((int)HttpStatusCode.NotFound, ((HttpStatusCodeResult)result).StatusCode);
        }
    }
}
