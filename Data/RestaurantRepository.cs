﻿using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using Data.Contract;
using System.Collections.Generic;

namespace Data
{
    public class RestaurantRepository : IRestaurantRepository
    {
        private readonly RestaurantDatabaseEntities _dbEntities;

        public RestaurantRepository(RestaurantDatabaseEntities dbEntities)
        {
            _dbEntities = dbEntities;
        }

        public IEnumerable<Restaurant> GetAll()
        {
            return _dbEntities.Restaurants;
        }

        public Restaurant GetById(int id)
        {
            return _dbEntities.Restaurants.FirstOrDefault(r => r.Id == id);
        }

        public void Update(Restaurant restaurant)
        {
            DbEntityEntry dbEntityEntry = _dbEntities.Entry(restaurant);
            if (dbEntityEntry.State == EntityState.Detached)
            {
                _dbEntities.Restaurants.Attach(restaurant);
            }  
            dbEntityEntry.State = EntityState.Modified;

            _dbEntities.SaveChanges();
        }
    }
}
