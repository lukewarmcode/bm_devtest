﻿using System.Collections.Generic;

namespace Data.Contract
{
    public interface IRestaurantRepository
    {
        IEnumerable<Restaurant> GetAll();
        Restaurant GetById(int id);
        void Update(Restaurant restaurant);
    }
}
