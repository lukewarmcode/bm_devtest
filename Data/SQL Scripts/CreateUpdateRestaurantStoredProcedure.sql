
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE UpdateRestaurant
	@Id int,
	@Title varchar(500),
    @PhoneNumberText varchar(50),
    @PhoneNumberKey bigint,
    @CuisineId int,
    @HeadChefName varchar(500),
    @RatingFood tinyint,
    @RatingWine tinyint,
    @StreetAddress varchar(500),
    @AddressSuburb varchar(500),
    @AddressState varchar(3),
    @AddressPostCode varchar(4),
    @Latitude float,
    @Longitude float,
    @WebAddress varchar(500),
    @ReviewText varchar(max),
    @Photo varbinary(max)
AS
BEGIN

	UPDATE [dbo].[Restaurant]
		SET [Title] = @Title
		   ,[PhoneNumberText] = @PhoneNumberText
		   ,[PhoneNumberKey] = @PhoneNumberKey
		   ,[CuisineId] = @CuisineId
		   ,[HeadChefName] = @HeadChefName
		   ,[RatingFood] = @RatingFood
		   ,[RatingWine] = @RatingWine
		   ,[StreetAddress] = @StreetAddress
		   ,[AddressSuburb] = @AddressSuburb
		   ,[AddressState] = @AddressState
		   ,[AddressPostCode] = @AddressPostCode
		   ,[Latitude] = @Latitude
		   ,[Longitude] = @Longitude
		   ,[WebAddress] = @WebAddress
		   ,[ReviewText] = @ReviewText
		   ,[Photo] = @Photo
	WHERE [Id] = @Id

END
GO
