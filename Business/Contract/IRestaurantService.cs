﻿using Business.DomainModel;
using System.Collections.Generic;

namespace Business.Contract
{
    public interface IRestaurantService
    {
        IEnumerable<Restaurant> GetAll();
        Restaurant GetById(int id);
        void Update(Restaurant restaurant);
    }
}
