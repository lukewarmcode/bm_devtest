﻿using AutoMapper;
using Business.Contract;
using Business.DomainModel;
using Data.Contract;
using System.Collections.Generic;
using System.Linq;

namespace Business.Services
{
    public class RestaurantService : IRestaurantService
    {
        private readonly IRestaurantRepository _restaurantRepository;

        public RestaurantService(IRestaurantRepository restaurantRepository)
        {
            _restaurantRepository = restaurantRepository;
        }

        static RestaurantService()
        {
            // Setup domain <-> data mappings

            Mapper.CreateMap<Data.Restaurant, DomainModel.Restaurant>()
                .ForMember(dest => dest.Cuisine, o => o.ResolveUsing(r => (CuisineEnum) r.CuisineId));

            Mapper.CreateMap<DomainModel.Restaurant, Data.Restaurant>()
                .ForMember(dest => dest.CuisineId, o => o.ResolveUsing(r => (int)r.Cuisine));
        }

        public IEnumerable<Restaurant> GetAll()
        {
            return _restaurantRepository.GetAll().Select(Mapper.Map<Restaurant>);
        }

        public Restaurant GetById(int id)
        {
            var restaurant = _restaurantRepository.GetById(id);

            return Mapper.Map<Restaurant>(restaurant);
        }

        public void Update(Restaurant restaurant)
        {
            restaurant.FormatPhoneNumberText();
            restaurant.CalculatePhoneNumberKey();

            var restaurantDataEntity = Mapper.Map<Data.Restaurant>(restaurant);

            _restaurantRepository.Update(restaurantDataEntity);
        }
    }
}
