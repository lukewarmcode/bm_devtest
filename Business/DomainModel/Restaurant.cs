﻿using System;
using System.Text.RegularExpressions;

namespace Business.DomainModel
{
    public class Restaurant
    {
        public const string PhoneNumberRegEx =
            @".*?([23478]).*?(\d).*?(\d).*?(\d).*?(\d).*?(\d).*?(\d).*?(\d).*?(\d).*";

        public int Id { get; set; }
        public string Title { get; set; }

        /// <summary>
        /// Should always be stored in format: (00) 0000 0000
        /// Calculates
        /// </summary>
        public string PhoneNumberText { get; set; }

        /// <summary>
        /// Phone number as entered into UI is converted to a long here, for use as a natural key
        /// </summary>
        public long PhoneNumberKey { get; set; }

        public CuisineEnum Cuisine { get; set; }
        public string HeadChefName { get; set; }
        
        /// <summary>
        /// "star-ratings" for food/wine must be in range 0 - 3
        /// </summary>
        public byte? RatingFood { get; set; }
        public byte? RatingWine { get; set; }

        public string StreetAddress { get; set; }
        public string AddressSuburb { get; set; }
        public string AddressState { get; set; }
        public string AddressPostCode { get; set; }
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }
        public string WebAddress { get; set; }
        public string ReviewText { get; set; }

        public static string FormatPhoneNumber(string phoneNumber)
        {
            if (String.IsNullOrEmpty(phoneNumber))
                throw new ArgumentException("phoneNumber");

            var match = Regex.Match(phoneNumber, PhoneNumberRegEx);

            if (match.Groups.Count < 10)
                throw new ArgumentException("phoneNumber");

            return String.Format("(0{0}) {1}{2}{3}{4} {5}{6}{7}{8}",
                                 match.Groups[1], match.Groups[2], match.Groups[3], match.Groups[4],
                                 match.Groups[5], match.Groups[6], match.Groups[7], match.Groups[8],
                                 match.Groups[9]);
        }

        public void FormatPhoneNumberText()
        {
            PhoneNumberText = FormatPhoneNumber(PhoneNumberText);
        }

        public void CalculatePhoneNumberKey()
        {
            PhoneNumberKey = -1;

            if (String.IsNullOrEmpty(PhoneNumberText))
                throw new Exception("Invalid model state");

            var match = Regex.Match(PhoneNumberText, PhoneNumberRegEx);

            if (match.Groups.Count < 10)
                throw new Exception("Invalid model state");

            var number =
                String.Format("{0}{1}{2}{3}{4}{5}{6}{7}{8}",
                              match.Groups[1], match.Groups[2], match.Groups[3], match.Groups[4],
                              match.Groups[5], match.Groups[6], match.Groups[7], match.Groups[8],
                              match.Groups[9]);

            PhoneNumberKey = Int64.Parse(number);
        }
    }
}
