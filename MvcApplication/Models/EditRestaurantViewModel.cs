﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using AutoMapper;
using Business.DomainModel;
using System.ComponentModel.DataAnnotations;

namespace MvcApplication.Models
{
    public class EditRestaurantViewModel
    {
        [Required]
        public int Id { get; set; }

        [Required(ErrorMessage = "Title is required")]
        [MaxLength(500, ErrorMessage = "Title length cannot exceed 500")]
        public string Title { get; set; }

        /// <summary>
        /// Should always be stored in format: (00) 0000 0000
        /// </summary>
        [Display(Name = "Phone Number")]
        [Required(ErrorMessage = "Phone Number is required")]
        [MaxLength(50, ErrorMessage = "Phone Number length cannot exceed 50")] // arbitrary pre-parsing max length
        [RegularExpression(Restaurant.PhoneNumberRegEx, 
            ErrorMessage = "Invalid Phone Number - only numbers within Australian area codes (02, 03, 04, 07, 08) are accepted")]
        public string PhoneNumberText { get; set; }

        [Required(ErrorMessage = "Cuisine is required")]
        public CuisineEnum Cuisine { get; set; }

        [Display(Name = "Head Chef Name")]
        [Required(ErrorMessage = "Head Chef Name is required")]
        [MaxLength(500, ErrorMessage = "Head Chef Name length cannot exceed 500")]
        public string HeadChefName { get; set; }

        [Display(Name = "Rating Food")]
        public byte? RatingFood { get; set; }

        [Display(Name = "Rating Wine")]
        public byte? RatingWine { get; set; }

        [Display(Name = "Web Address")]
        [MaxLength(500, ErrorMessage = "Web Address length cannot exceed 500")]
        public string WebAddress { get; set; }

        // TODO confirm max length
        [Display(Name = "Review")]
        [MaxLength(2000, ErrorMessage = "Review length length cannot exceed 2000")]
        public string ReviewText { get; set; }

        [Display(Name = "Street")]
        [MaxLength(500, ErrorMessage = "Street length cannot exceed 500")]
        public string StreetAddress { get; set; }

        [Display(Name = "Suburb")]
        [MaxLength(500, ErrorMessage = "Suburb length cannot exceed 500")]
        public string AddressSuburb { get; set; }

        [Display(Name = "State")]
        [MaxLength(500, ErrorMessage = "State length cannot exceed 500")]
        public string AddressState { get; set; }

        [Display(Name = "Postcode")]
        [MaxLength(500, ErrorMessage = "Postcode length cannot exceed 500")]
        public string AddressPostCode { get; set; }

        public double? Latitude { get; set; }

        public double? Longitude { get; set; }

        public string ConfirmationMessage { get; set; }

        //public IEnumerable<SelectListItem> CuisineSelectListItems { get; set; }

        //public EditRestaurantViewModel()
        //{
        //    // Populate Cuisine Select List Items
        //    CuisineSelectListItems =
        //        Enum.GetValues(typeof(CuisineEnum))
        //            .OfType<CuisineEnum>()
        //            .Select(val => new SelectListItem
        //            {
        //                Text = Enum.GetName(typeof(CuisineEnum), val),
        //                Value = ((int)val).ToString()
        //            })
        //            .ToList();
        //}

        static EditRestaurantViewModel()
        {
            Mapper.CreateMap<Restaurant, EditRestaurantViewModel>();
            Mapper.CreateMap<EditRestaurantViewModel, Restaurant>();
        }

        public static EditRestaurantViewModel FromRestaurantDomainModel(Restaurant restaurant)
        {
            var vm = Mapper.Map<EditRestaurantViewModel>(restaurant);

            //var cuisineId = vm.Cuisine.ToString();

            //var itemToSelect = vm.CuisineSelectListItems.FirstOrDefault(item => item.Value == cuisineId);

            //if (itemToSelect != null)
            //    itemToSelect.Selected = true;

            return vm;
        }

        public Restaurant ToDomainRestaurant()
        {
            return Mapper.Map<Restaurant>(this);
        }
    }
}