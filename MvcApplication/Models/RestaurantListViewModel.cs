﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Business.DomainModel;

namespace MvcApplication.Models
{
    public class RestaurantListViewModel
    {
        public IEnumerable<Restaurant> Restaurants { get; set; }
    }
}