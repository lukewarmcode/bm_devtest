﻿using Autofac;
using Autofac.Integration.Mvc;
using Business.Services;
using Data;

namespace MvcApplication
{
    public static class AutoFacConfig
    {
        public static IContainer GetContainer()
        {
            var builder = new ContainerBuilder();

            builder.RegisterControllers(typeof(MvcApplication).Assembly);

            builder.RegisterType<RestaurantDatabaseEntities>().AsSelf();

            builder.RegisterType<RestaurantRepository>().AsImplementedInterfaces();
            builder.RegisterType<RestaurantService>().AsImplementedInterfaces();

            return builder.Build();
        }
    }
}