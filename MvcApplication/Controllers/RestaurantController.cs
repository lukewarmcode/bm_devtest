﻿using System.Globalization;
using System.Linq;
using Business.Contract;
using Business.DomainModel;
using MvcApplication.Models;
using System.Net;
using System.Web.Mvc;

namespace MvcApplication.Controllers
{
    public class RestaurantController : Controller
    {
        private readonly IRestaurantService _restaurantService;

        public RestaurantController(IRestaurantService restaurantService)
        {
            _restaurantService = restaurantService;
        }

        //
        // GET: /Restaurant/
        public ActionResult Edit(int? id)
        {
            if (!id.HasValue)
                return new HttpStatusCodeResult(HttpStatusCode.NotFound);

            var restaurant = _restaurantService.GetById(id.Value);

            if (restaurant == null)
                return new HttpStatusCodeResult(HttpStatusCode.NotFound);

            var vm = EditRestaurantViewModel.FromRestaurantDomainModel(restaurant);

            return View(vm);
        }

        [HttpPost]
        public ActionResult Edit(EditRestaurantViewModel vm)
        {
            if (!ModelState.IsValid)
            {
                // Enforce formatting of phone number
                if (!ModelState["PhoneNumberText"].Errors.Any())
                {
                    var formattedPhoneNumber = Restaurant.FormatPhoneNumber(vm.PhoneNumberText);
                    var vpr = new ValueProviderResult(formattedPhoneNumber, formattedPhoneNumber, CultureInfo.CurrentCulture);
                    ModelState["PhoneNumberText"].Value = vpr;
                }

                return View(vm);
            }

            _restaurantService.Update(vm.ToDomainRestaurant());

            // Read back entity
            var updatedRestaurant = _restaurantService.GetById(vm.Id);
            vm = EditRestaurantViewModel.FromRestaurantDomainModel(updatedRestaurant);

            vm.ConfirmationMessage = "Update Successful!";

            ModelState.Clear();

            return View(vm);
        }
    }
}
